var mongoose = require('mongoose');

mongoose.Promise = global.Promise

mongoose.connect('mongodb://usuario:password@10.70.24.6/basedatos', { useMongoClient: true });
 
var db = mongoose.connection;

// Se define la estructura que va a tener cada uno de nuestros registros
var LibroSchema = mongoose.Schema({
  nombre: String,
  isbn: {type: String, index: true},
  autor: String,
  paginas: Number
});

// Se asocia la estructura con una coleccion
var Libro = mongoose.model('Libro', LibroSchema, "libros");
 
db.on('error', function (err) {
   console.log('Error de conexion', err);
});

// Se abre la base de datos
db.once('open', function () {
   console.log('connectado.');	

	var libro1 = new Libro({
	    nombre:"Tutorial de Mongo",
	    isbn: "MNG123",
	    autor: "Autor1,  Autor2",
	    paginas: 123
	});

	//Guardando la instancia en la BD
	libro1.save(function(err){
	    if ( err ) throw err;
	    console.log("Libro guardado exitosamente");
	    queryLibros(); // ahora se hace una consulta
	});

});

var queryLibros = function(){
  //Ahora se hace una consulta de los libros con mas de 100 paginas  
  Libro.find({paginas : {$gt:100}}, "nombre isbn autor paginas", function(err, result){
    if ( err ) throw err;
    console.log("Operacion de consulta: " + result);
  });
}